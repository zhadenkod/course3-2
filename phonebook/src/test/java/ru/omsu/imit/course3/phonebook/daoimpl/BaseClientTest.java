package ru.omsu.imit.course3.phonebook.daoimpl;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.phonebook.client.PhonebookClient;
import ru.omsu.imit.course3.phonebook.models.PhoneNumberType;
import ru.omsu.imit.course3.phonebook.rest.request.ContactRequest;
import ru.omsu.imit.course3.phonebook.rest.request.PhoneNumberRequest;
import ru.omsu.imit.course3.phonebook.rest.request.SectionRequest;
import ru.omsu.imit.course3.phonebook.rest.responce.*;
import ru.omsu.imit.course3.phonebook.server.PhonebookServer;
import ru.omsu.imit.course3.phonebook.server.config.Settings;
import ru.omsu.imit.course3.phonebook.utils.ErrorCode;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BaseClientTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseClientTest.class);

    protected static PhonebookClient client = new PhonebookClient();
    private static String baseURL;

    private static void initialize() {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            LOGGER.debug("Can't determine my own host name", e);
        }
        baseURL = "http://" + hostName + ":" + Settings.getRestHTTPPort() + "/api";
    }

    @BeforeClass
    public static void startServer() {
        initialize();
        PhonebookServer.createServer();
    }

    @AfterClass
    public static void stopServer() {
        PhonebookServer.stopServer();
    }

    @Before
    public void clearDataBase() {
        SectionDAOImpl sectionDAO = new SectionDAOImpl();
        ContactDAOImpl contactDAO = new ContactDAOImpl();
        PhoneNumberDAOImpl phoneNumberDAO = new PhoneNumberDAOImpl();
        sectionDAO.deleteAll();
        contactDAO.deleteAll();
        phoneNumberDAO.deleteAll();
    }

    public static String getBaseURL() {
        return baseURL;
    }

    protected void checkFailureResponse(Object response, ErrorCode expectedStatus) {
        assertTrue(response instanceof FailureResponse);
        FailureResponse failureResponseObject = (FailureResponse) response;
        assertEquals(expectedStatus, failureResponseObject.getErrorCode());
    }

    protected SectionInfoResponse addSection(String name, ErrorCode expectedStatus) {
        SectionRequest request = new SectionRequest(name);
        Object response = client.post(baseURL + "/sections", request, SectionInfoResponse.class);
        if (response instanceof SectionInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            SectionInfoResponse addSectionResponse = (SectionInfoResponse) response;
            assertEquals(name, addSectionResponse.getName());
            return addSectionResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected SectionInfoResponse getSectionById(int id, String expectedName, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/sections/" + id, SectionInfoResponse.class);
        if (response instanceof SectionInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            SectionInfoResponse getSectionResponse = (SectionInfoResponse) response;
            assertEquals(id, getSectionResponse.getId());
            assertEquals(expectedName, getSectionResponse.getName());
            return getSectionResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<SectionInfoResponse> getAllSections(int expectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/sections/", List.class);
        if (response instanceof List<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<SectionInfoResponse> responseList = (List<SectionInfoResponse>) response;
            assertEquals(expectedCount, responseList.size());
            return responseList;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected SectionInfoResponse editSectionById(int id, String name, ErrorCode expectedStatus) {
        SectionRequest request = new SectionRequest(name);
        Object response = client.put(baseURL + "/sections/" + id , request, SectionInfoResponse.class);
        if (response instanceof SectionInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            SectionInfoResponse addSectionResponse = (SectionInfoResponse) response;
            assertEquals(name, addSectionResponse.getName());
            return addSectionResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteSectionById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/sections/" + id , EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse)response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected ContactInfoResponse addContact(String firstname, String lastname, int sectionId, ErrorCode expectedStatus) {
        ContactRequest request = new ContactRequest(firstname, lastname, sectionId);
        Object response = client.post(baseURL + "/contacts", request, ContactInfoResponse.class);
        if (response instanceof ContactInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            ContactInfoResponse addContactResponse = (ContactInfoResponse) response;
            assertEquals(firstname, addContactResponse.getFirstname());
            assertEquals(lastname, addContactResponse.getLastname());
            assertEquals(sectionId, addContactResponse.getSection_id());
            return addContactResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected ContactInfoResponse getContactById(int id, String expectedFirstname, String expectedLastname,
                                                 int expectedSectionId, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/contacts/" + id, ContactInfoResponse.class);
        if (response instanceof ContactInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            ContactInfoResponse getContactResponse = (ContactInfoResponse) response;
            assertEquals(id, getContactResponse.getId());
            assertEquals(expectedFirstname, getContactResponse.getFirstname());
            assertEquals(expectedLastname, getContactResponse.getLastname());
            assertEquals(expectedSectionId, getContactResponse.getSection_id());
            return getContactResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<ContactInfoResponse> getAllContacts(int expectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/contacts/", List.class);
        if (response instanceof List<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<ContactInfoResponse> responseList = (List<ContactInfoResponse>) response;
            assertEquals(expectedCount, responseList.size());
            return responseList;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected ContactInfoResponse editContactById(int id, String firstname, String lastname, int sectionId, ErrorCode expectedStatus) {
        ContactRequest request = new ContactRequest(firstname, lastname, sectionId);
        Object response = client.put(baseURL + "/contacts/" + id , request, ContactInfoResponse.class);
        if (response instanceof ContactInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            ContactInfoResponse addContactResponse = (ContactInfoResponse) response;
            assertEquals(firstname, addContactResponse.getFirstname());
            assertEquals(lastname, addContactResponse.getLastname());
            assertEquals(sectionId, addContactResponse.getSection_id());
            return addContactResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteContactById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/contacts/" + id , EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse)response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected PhoneNumberInfoResponse addPhoneNumber(String phoneNumber, PhoneNumberType type, int contactId, ErrorCode expectedStatus) {
        PhoneNumberRequest request = new PhoneNumberRequest(phoneNumber, type, contactId);
        Object response = client.post(baseURL + "/phonenumbers", request, PhoneNumberInfoResponse.class);
        if (response instanceof PhoneNumberInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PhoneNumberInfoResponse addPhoneNumberResponse = (PhoneNumberInfoResponse) response;
            assertEquals(phoneNumber, addPhoneNumberResponse.getPhoneNumber());
            assertEquals(type.toString(), addPhoneNumberResponse.getPhoneNumberType());
            assertEquals(contactId, addPhoneNumberResponse.getContact_id());
            return addPhoneNumberResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected PhoneNumberInfoResponse getPhoneNumberById(int id, String expectedPhoneNumber, PhoneNumberType expectedPhoneType,
                                                 int expectedContactId, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/phonenumbers/" + id, PhoneNumberInfoResponse.class);
        if (response instanceof PhoneNumberInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PhoneNumberInfoResponse getPhoneNumberResponse = (PhoneNumberInfoResponse) response;
            assertEquals(id, getPhoneNumberResponse.getId());
            assertEquals(expectedPhoneNumber, getPhoneNumberResponse.getPhoneNumber());
            assertEquals(expectedPhoneType.toString(), getPhoneNumberResponse.getPhoneNumberType());
            assertEquals(expectedContactId, getPhoneNumberResponse.getContact_id());
            return getPhoneNumberResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<PhoneNumberInfoResponse> getAllPhoneNumbers(int expectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/phonenumbers/", List.class);
        if (response instanceof List<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<PhoneNumberInfoResponse> responseList = (List<PhoneNumberInfoResponse>) response;
            assertEquals(expectedCount, responseList.size());
            return responseList;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected PhoneNumberInfoResponse editPhoneNumberById(int id, String phoneNumber, PhoneNumberType type, int contactId, ErrorCode expectedStatus) {
        PhoneNumberRequest request = new PhoneNumberRequest(phoneNumber, type, contactId);
        Object response = client.put(baseURL + "/phonenumbers/" + id , request, PhoneNumberInfoResponse.class);
        if (response instanceof PhoneNumberInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PhoneNumberInfoResponse addPhoneNumberResponse = (PhoneNumberInfoResponse) response;
            assertEquals(phoneNumber, addPhoneNumberResponse.getPhoneNumber());
            assertEquals(type.toString(), addPhoneNumberResponse.getPhoneNumberType());
            assertEquals(contactId, addPhoneNumberResponse.getContact_id());
            return addPhoneNumberResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deletePhoneNumberById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/phonenumbers/" + id , EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse)response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

}
