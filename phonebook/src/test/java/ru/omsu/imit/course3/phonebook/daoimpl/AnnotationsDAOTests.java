package ru.omsu.imit.course3.phonebook.daoimpl;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.course3.phonebook.dao.PhoneNumberDAO;
import ru.omsu.imit.course3.phonebook.models.Contact;
import ru.omsu.imit.course3.phonebook.models.PhoneNumber;
import ru.omsu.imit.course3.phonebook.models.PhoneNumberType;
import ru.omsu.imit.course3.phonebook.models.Section;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNull;

public class AnnotationsDAOTests extends BaseDAOTests {
    @Test
    public void testInsertSection() throws PhonebookException {
        Section section = new Section(1, "Друзья");
        sectionDAO.insert(section);
        Section sectionFromDB = sectionDAO.getById(section.getId());
        checkSectionFields(section, sectionFromDB);
    }

    @Test(expected = RuntimeException.class)
    public void testInsertSectionWithNull() {
        Section section = new Section(1, null);
        sectionDAO.insert(section);
    }

    @Test
    public void testSectionGetAll() {
        List<Section> list = new ArrayList<>();
        Section sectionOne = new Section(1, "Друзья");
        Section sectionTwo = new Section(2, "Родственники");
        list.add(sectionOne);
        list.add(sectionTwo);
        sectionDAO.insert(sectionOne);
        sectionDAO.insert(sectionTwo);
        List<Section> allSectionsFromDB = sectionDAO.getAll();
        Assert.assertEquals(list, allSectionsFromDB);
    }

    @Test
    public void testSectionDeleteAll() {
        List<Section> list = new ArrayList<>();
        Section sectionOne = new Section(1, "Друзья");
        Section sectionTwo = new Section(2, "Родственники");
        sectionDAO.insert(sectionOne);
        sectionDAO.insert(sectionTwo);
        sectionDAO.deleteAll();
        Assert.assertEquals(new ArrayList<>(), sectionDAO.getAll());
    }

    @Test
    public void testInsertContact() throws PhonebookException {
        Section section = new Section(1, "Друзья");
        sectionDAO.insert(section);
        Contact contact = new Contact(1, "Борис",
                "Кот", section);
        PhoneNumber phoneNumber = new PhoneNumber(1, "11-11-11",
                PhoneNumberType.HOME, null);
        List<PhoneNumber> phoneNumbers = new ArrayList<>();
        phoneNumbers.add(phoneNumber);
        contact.setPhoneNumbers(phoneNumbers);
        int contactId = contactDAO.insert(contact).getId();
        phoneNumber.setContact(contact);
        phoneNumberDAO.insert(phoneNumber);
        Contact contactFromDB = contactDAO.getById(contactId);
        System.out.println(contactFromDB);
        System.out.println(contact);
        checkContactFields(contact, contactFromDB);
    }

    @Test
    public void testInsertPhoneNumber() throws PhonebookException {
        Section section = new Section(1, "Друзья");
        sectionDAO.insert(section);
        Contact contact = new Contact(1, "Борис",
                "Кот", section);
        contactDAO.insert(contact);
        PhoneNumber phoneNumber = new PhoneNumber(1, "11-11-11",
                PhoneNumberType.HOME, contact);
        int phoneNumberId = phoneNumberDAO.insert(phoneNumber).getId();
        PhoneNumber phoneNumberFromDB = phoneNumberDAO.getById(phoneNumberId);
        checkPhoneNumberFields(phoneNumber, phoneNumberFromDB);
    }

    @Test
    public void testGetPhoneNumberByContact() throws PhonebookException {
        Section section = new Section(1, "Друзья");
        sectionDAO.insert(section);
        Contact contact = new Contact(1, "Борис",
                "Кот", section);
        int contactId = contactDAO.insert(contact).getId();
        PhoneNumber phoneNumber = new PhoneNumber(1, "11-11-11",
                PhoneNumberType.HOME, contact);
        int phoneNumberId = phoneNumberDAO.insert(phoneNumber).getId();
        PhoneNumber phoneNumberFromDB = phoneNumberDAO.getByContact(contactId);
        checkPhoneNumberFields(phoneNumber, phoneNumberFromDB);
    }

    @Test(expected = PhonebookException.class)
    public void testDeleteContactById() throws PhonebookException {
        Section section = new Section(1, "Друзья");
        sectionDAO.insert(section);
        Contact contact = new Contact(1, "Борис",
                "Кот", section);
        int contactId = contactDAO.insert(contact).getId();
        contactDAO.deleteById(contactId);
        Contact contactFromDB = contactDAO.getById(contactId);
        assertNull(contactFromDB);
    }

    @Test(expected = PhonebookException.class)
    public void testDeletePhoneNumber() throws PhonebookException {
        Section section = new Section(1, "Друзья");
        sectionDAO.insert(section);
        Contact contact = new Contact(1, "Борис",
                "Кот", section);
        contactDAO.insert(contact);
        PhoneNumber phoneNumber = new PhoneNumber(1, "11-11-11",
                PhoneNumberType.HOME, contact);
        int phoneNumberId = phoneNumberDAO.insert(phoneNumber).getId();
        phoneNumberDAO.deleteById(phoneNumberId);
        PhoneNumber phoneNumberFromDB = phoneNumberDAO.getById(phoneNumberId);
        assertNull(phoneNumberFromDB);
    }

}
