package ru.omsu.imit.course3.phonebook.daoimpl;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import ru.omsu.imit.course3.phonebook.dao.CommonDAO;
import ru.omsu.imit.course3.phonebook.dao.ContactDAO;
import ru.omsu.imit.course3.phonebook.dao.PhoneNumberDAO;
import ru.omsu.imit.course3.phonebook.dao.SectionDAO;
import ru.omsu.imit.course3.phonebook.models.Contact;
import ru.omsu.imit.course3.phonebook.models.PhoneNumber;
import ru.omsu.imit.course3.phonebook.models.Section;
import ru.omsu.imit.course3.phonebook.utils.MyBatisUtils;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class BaseDAOTests {
    protected CommonDAO commonDAO = new CommonDAOImpl();
    protected SectionDAO sectionDAO = new SectionDAOImpl();
    protected ContactDAO contactDAO = new ContactDAOImpl();
    protected PhoneNumberDAO phoneNumberDAO = new PhoneNumberDAOImpl();

    @BeforeClass()
    public static void init() {
        Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
    }

    @Before()
    public void clearDatabase() {
        sectionDAO.deleteAll();
        contactDAO.deleteAll();
        phoneNumberDAO.deleteAll();
    }

    protected void checkSectionFields(Section section1, Section section2) {
        assertEquals(section1.getId(), section2.getId());
        assertEquals(section1.getName(), section2.getName());
    }

    protected void checkPhoneNumberFields(PhoneNumber phoneNumber1, PhoneNumber phoneNumber2) {
        assertEquals(phoneNumber1.getId(), phoneNumber2.getId());
        assertEquals(phoneNumber1.getType(), phoneNumber2.getType());
        assertEquals(phoneNumber1.getPhoneNumber(), phoneNumber2.getPhoneNumber());
    }

    protected void checkContactFields(Contact contact1, Contact contact2) {
        assertEquals(contact1.getId(), contact2.getId());
        assertEquals(contact1.getFirstname(), contact2.getFirstname());
        assertEquals(contact1.getLastname(), contact2.getLastname());
        assertEquals(contact1.getPhoneNumbers().size(), contact2.getPhoneNumbers().size());
        for (int i = 0; i < contact1.getPhoneNumbers().size(); i++) {
            checkPhoneNumberFields(contact1.getPhoneNumbers().get(i), contact2.getPhoneNumbers().get(i));
        }
    }
}
