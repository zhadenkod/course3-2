package ru.omsu.imit.course3.phonebook.daoimpl;

import org.junit.Test;
import ru.omsu.imit.course3.phonebook.models.PhoneNumberType;
import ru.omsu.imit.course3.phonebook.rest.responce.ContactInfoResponse;
import ru.omsu.imit.course3.phonebook.rest.responce.PhoneNumberInfoResponse;
import ru.omsu.imit.course3.phonebook.rest.responce.SectionInfoResponse;
import ru.omsu.imit.course3.phonebook.utils.ErrorCode;

public class ClientTest extends BaseClientTest {

    @Test
    public void testAddSection() {
        addSection("Семья", ErrorCode.SUCCESS);
    }

    @Test
    public void testGetSectionById() {
        SectionInfoResponse response = addSection("Друзья", ErrorCode.SUCCESS);
        getSectionById(response.getId(), "Друзья", ErrorCode.SUCCESS);
    }

    @Test
    public void testGetSectionByWrongId() {
        getSectionById(-3, "Друзья", ErrorCode.SECTION_NOT_FOUND);
    }

    @Test
    public void testGetAllSections() {
        addSection("Семья", ErrorCode.SUCCESS);
        addSection("Друзья", ErrorCode.SUCCESS);
        addSection("Работа", ErrorCode.SUCCESS);
        getAllSections(3, ErrorCode.SUCCESS);
    }

    @Test
    public void testGetAllSectionsWhenEmpty() {
        getAllSections(0, ErrorCode.SUCCESS);
    }

    @Test
    public void testEditSectionById() {
        SectionInfoResponse response = addSection("Работа", ErrorCode.SUCCESS);
        editSectionById(response.getId(), "Коллеги", ErrorCode.SUCCESS);
    }

    @Test
    public void testEditSectionWithWrongId() {
        editSectionById(-3, "Коллеги", ErrorCode.SECTION_NOT_FOUND);
    }

    @Test
    public void testDeleteSectionById() {
        SectionInfoResponse response = addSection("Работа", ErrorCode.SUCCESS);
        deleteSectionById(response.getId(), ErrorCode.SUCCESS);
    }

    @Test
    public void testAddContact() {
        int sectionId = addSection("Семья", ErrorCode.SUCCESS).getId();
        addContact("Борис", "Кот", sectionId, ErrorCode.SUCCESS).getId();
    }

    @Test
    public void testGetContactById() {
        int sectionId = addSection("Семья", ErrorCode.SUCCESS).getId();
        int contactId = addContact("Борис", "Кот", sectionId, ErrorCode.SUCCESS).getId();
        getContactById(contactId, "Борис", "Кот", sectionId, ErrorCode.SUCCESS);
    }

    @Test
    public void testGetAllContacts() {
        int sectionId = addSection("Семья", ErrorCode.SUCCESS).getId();
        addContact("Василий", "Васильев", sectionId, ErrorCode.SUCCESS);
        addContact("Иван", "Иванов", sectionId, ErrorCode.SUCCESS);
        addContact("Петр", "Петров", sectionId, ErrorCode.SUCCESS);
        getAllContacts(3, ErrorCode.SUCCESS);
    }

    @Test
    public void testGetAllContactsWhenEmpty() {
        getAllContacts(0, ErrorCode.SUCCESS);
    }

    @Test
    public void testEditContactById() {
        int sectionId = addSection("Работа", ErrorCode.SUCCESS).getId();
        int contactId = addContact("Василий", "Васильев", sectionId, ErrorCode.SUCCESS).getId();
        editContactById(contactId, "Борис", "Кот", sectionId, ErrorCode.SUCCESS);
    }

    @Test
    public void testEditContactWithWrongId() {
        editContactById(-3, "Борис", "Кот", 1, ErrorCode.CONTACT_NOT_FOUND);
    }

    @Test
    public void testDeleteContactById() {
        int sectionId = addSection("Работа", ErrorCode.SUCCESS).getId();
        ContactInfoResponse response = addContact("Борис", "Кот", sectionId, ErrorCode.SUCCESS);
        deleteContactById(response.getId(), ErrorCode.SUCCESS);
    }

    @Test
    public void testAddPhoneNumber() {
        int sectionId = addSection("Семья", ErrorCode.SUCCESS).getId();
        int contactId = addContact("Борис", "Кот", sectionId, ErrorCode.SUCCESS).getId();
        addPhoneNumber("12345678", PhoneNumberType.HOME, contactId, ErrorCode.SUCCESS);
    }

    @Test
    public void testGetPhoneNumberById() {
        int sectionId = addSection("Семья", ErrorCode.SUCCESS).getId();
        int contactId = addContact("Борис", "Кот", sectionId, ErrorCode.SUCCESS).getId();
        int phoneNumberId = addPhoneNumber("12345678", PhoneNumberType.HOME, contactId, ErrorCode.SUCCESS).getId();
        getPhoneNumberById(phoneNumberId, "12345678", PhoneNumberType.HOME, contactId, ErrorCode.SUCCESS);
    }

    @Test
    public void testGetAllPhoneNumbers() {
        int sectionId = addSection("Семья", ErrorCode.SUCCESS).getId();
        int contactId = addContact("Борис", "Кот", sectionId, ErrorCode.SUCCESS).getId();
        addPhoneNumber("12345678", PhoneNumberType.HOME, contactId, ErrorCode.SUCCESS);
        addPhoneNumber("89007771122", PhoneNumberType.MOBILE, contactId, ErrorCode.SUCCESS);
        addPhoneNumber("404050", PhoneNumberType.WORK, contactId, ErrorCode.SUCCESS);
        getAllPhoneNumbers(3, ErrorCode.SUCCESS);
    }

    @Test
    public void testGetAllPhoneNumbersWhenEmpty() {
        getAllPhoneNumbers(0, ErrorCode.SUCCESS);
    }

    @Test
    public void testEditPhoneNumberById() {
        int sectionId = addSection("Работа", ErrorCode.SUCCESS).getId();
        int contactId = addContact("Василий", "Васильев", sectionId, ErrorCode.SUCCESS).getId();
        int phoneNumberId = addPhoneNumber("12345678", PhoneNumberType.HOME, contactId, ErrorCode.SUCCESS).getId();
        editPhoneNumberById(phoneNumberId, "87654321", PhoneNumberType.HOME, contactId, ErrorCode.SUCCESS);
    }

    @Test
    public void testEditPhoneNumberWithWrongId() {
        editPhoneNumberById(-3, "87654321", PhoneNumberType.HOME, 1, ErrorCode.PHONENUMBER_NOT_FOUND);
    }

    @Test
    public void testDeletePhoneNumberById() {
        int sectionId = addSection("Работа", ErrorCode.SUCCESS).getId();
        int contactId = addContact("Борис", "Кот", sectionId, ErrorCode.SUCCESS).getId();
        PhoneNumberInfoResponse response = addPhoneNumber("12345678", PhoneNumberType.HOME, contactId, ErrorCode.SUCCESS);
        deletePhoneNumberById(response.getId(), ErrorCode.SUCCESS);
    }
}
