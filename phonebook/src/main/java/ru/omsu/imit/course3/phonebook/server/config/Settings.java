package ru.omsu.imit.course3.phonebook.server.config;

public class Settings {
	
	private static int restHttpPort = 8080;

	public static int getRestHTTPPort() {
		return restHttpPort;
	}

}
