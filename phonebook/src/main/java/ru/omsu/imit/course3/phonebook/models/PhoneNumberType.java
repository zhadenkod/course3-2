package ru.omsu.imit.course3.phonebook.models;

public enum PhoneNumberType {
    MOBILE("Мобильный"), HOME("Домашний"), WORK("Рабочий");
    private String explanation;

    PhoneNumberType(String explanation) {
        this.explanation = explanation;
    }

    public String getExplanation() {
        return explanation;
    }
}
