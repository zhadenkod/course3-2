package ru.omsu.imit.course3.phonebook.utils;

public enum ErrorCode {
    SUCCESS("", ""),
    SECTION_NOT_FOUND("section", "Section not found %s"),
    CONTACT_NOT_FOUND("contact", "Contact not found %s"),
    PHONENUMBER_NOT_FOUND("phoneNumber", "Phone number not found %s"),
    NULL_REQUEST("json", "Null request"),
    JSON_PARSE_EXCEPTION("json", "Json parse exception :  %s"),
    WRONG_URL("url", "Wrong URL"),
    METHOD_NOT_ALLOWED("url", "Method not allowed")
    ;

    private String field;
    private String message;

    private ErrorCode(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
}
