package ru.omsu.imit.course3.phonebook.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Section {
    private int id;
    private String name;
    private List<Contact> contacts;

    public Section(int id, String name) {
        this.id = id;
        this.name = name;
        this.contacts = new ArrayList<>();
    }

    public Section(String name) {
        this.id = 0;
        this.name = name;
    }

    public Section() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    @Override
    public String toString() {
        return "Section{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", contacts=" + contacts +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return id == section.id &&
                Objects.equals(name, section.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
