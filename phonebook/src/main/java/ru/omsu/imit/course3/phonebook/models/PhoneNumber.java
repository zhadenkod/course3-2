package ru.omsu.imit.course3.phonebook.models;

import java.util.Objects;

public class PhoneNumber {
    private int id;
    private String phoneNumber;
    private PhoneNumberType type;
    private Contact contact;

    public PhoneNumber(int id, String phoneNumber, PhoneNumberType type, Contact contact) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.contact = contact;
    }

    public PhoneNumber(String phoneNumber, PhoneNumberType type, int contact) {
        this.id = 0;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.contact = new Contact(contact, "", "",
                new Section(0, ""));
    }

    public PhoneNumber() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public PhoneNumberType getType() {
        return type;
    }

    public void setType(PhoneNumberType type) {
        this.type = type;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "PhoneNumber{" +
                "id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneNumber that = (PhoneNumber) o;
        return id == that.id &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                type == that.type &&
                Objects.equals(contact, that.contact);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phoneNumber, type, contact);
    }
}
