package ru.omsu.imit.course3.phonebook.mappers;

import org.apache.ibatis.annotations.*;
import ru.omsu.imit.course3.phonebook.models.PhoneNumber;

import java.util.List;

public interface PhoneNumberMapper {
    @Insert("INSERT INTO phones (phone, type, contact_id) VALUES ( #{phoneNumber}, #{type}, #{contact.id} )")
    @Options(useGeneratedKeys = true)
    Integer insert(PhoneNumber phoneNumber);

    @Select("SELECT id, phone, type, contact_id FROM phones WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "contact.id", column = "contact_id"),
            @Result(property = "phoneNumber", column = "phone")
    })
    PhoneNumber getById(int id);

    @Select("SELECT id, phone, type, contact_id FROM phones WHERE contact_id = #{contact.id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "contact.id", column = "contact_id"),
            @Result(property = "phoneNumber", column = "phone")
    })
    PhoneNumber getByContact(int contact_id);

    @Select("SELECT * FROM phones")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "contact.id", column = "contact_id"),
            @Result(property = "phoneNumber", column = "phone")
    })
    List<PhoneNumber> getAll();

    @Update("UPDATE phones SET phone = #{phoneNumber} WHERE id = #{id}")
    void edit(@Param("id") int id, @Param("phoneNumber") String phoneNumber);

    @Delete("DELETE FROM phones WHERE id = #{id}")
    void deleteById(int id);

    @Delete("DELETE FROM phones")
    void deleteAll();
}
