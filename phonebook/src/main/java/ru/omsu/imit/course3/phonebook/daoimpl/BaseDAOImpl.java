package ru.omsu.imit.course3.phonebook.daoimpl;

import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.course3.phonebook.mappers.ContactMapper;
import ru.omsu.imit.course3.phonebook.mappers.PhoneNumberMapper;
import ru.omsu.imit.course3.phonebook.mappers.SectionMapper;
import ru.omsu.imit.course3.phonebook.utils.MyBatisUtils;

public class BaseDAOImpl {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected PhoneNumberMapper getPhoneNumberMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(PhoneNumberMapper.class);
    }

    protected ContactMapper getContactMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ContactMapper.class);
    }

    protected SectionMapper getSectionMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(SectionMapper.class);
    }

}