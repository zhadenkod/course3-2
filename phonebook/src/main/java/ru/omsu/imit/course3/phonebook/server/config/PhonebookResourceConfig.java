package ru.omsu.imit.course3.phonebook.server.config;

import org.glassfish.jersey.server.ResourceConfig;

public class PhonebookResourceConfig extends ResourceConfig {
    public PhonebookResourceConfig() {
        packages("ru.omsu.imit.course3.phonebook.resources",
                "ru.omsu.imit.course3.phonebook.rest.mappers");
    }
}
