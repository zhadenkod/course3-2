package ru.omsu.imit.course3.phonebook.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.phonebook.dao.PhoneNumberDAO;
import ru.omsu.imit.course3.phonebook.models.PhoneNumber;
import ru.omsu.imit.course3.phonebook.utils.ErrorCode;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;

import java.util.List;

public class PhoneNumberDAOImpl extends BaseDAOImpl implements PhoneNumberDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneNumberDAOImpl.class);

    @Override
    public PhoneNumber insert(PhoneNumber phoneNumber) {
        LOGGER.debug("DAO Insert PhoneNumber {}", phoneNumber);
        try (SqlSession sqlSession = getSession()) {
            try {
                getPhoneNumberMapper(sqlSession).insert(phoneNumber);
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't insert PhoneNumber {} {}", phoneNumber, exception);
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
        return phoneNumber;
    }

    @Override
    public PhoneNumber getById(int id) throws PhonebookException {
        try (SqlSession sqlSession = getSession()) {
            PhoneNumber phoneNumber = getPhoneNumberMapper(sqlSession).getById(id);
            if (phoneNumber == null) {
                throw new PhonebookException(ErrorCode.PHONENUMBER_NOT_FOUND, Integer.toString(id));
            }
            return phoneNumber;
        }
        catch (RuntimeException exception) {
            LOGGER.debug("Can't get PhoneNumber by Id {} {}", id, exception);
            throw exception;
        }
    }

    @Override
    public List<PhoneNumber> getAll() {
        try (SqlSession sqlSession = getSession()) {
            return getPhoneNumberMapper(sqlSession).getAll();
        }
        catch (RuntimeException exception) {
            LOGGER.debug("Can't get all PhoneNumbers", exception);
            throw exception;
        }
    }

    @Override
    public PhoneNumber getByContact(int contact_id) {
        try (SqlSession sqlSession = getSession()) {
            return getPhoneNumberMapper(sqlSession).getByContact(contact_id);
        }
        catch (RuntimeException exception) {
            LOGGER.debug("Can't get PhoneNumber by Contact {} {}", contact_id, exception);
            throw exception;
        }
    }

    @Override
    public PhoneNumber editPhoneNumber(int id, String phoneNumber) throws PhonebookException {
        PhoneNumber editedPhoneNumber;
        try (SqlSession sqlSession = getSession()) {
            getPhoneNumberMapper(sqlSession).edit(id, phoneNumber);
            editedPhoneNumber = getPhoneNumberMapper(sqlSession).getById(id);
            if (editedPhoneNumber == null) {
                LOGGER.debug("Can't get Contact by Id {}", id);
                throw new PhonebookException(ErrorCode.PHONENUMBER_NOT_FOUND, Integer.toString(id));
            }
        }
        catch (RuntimeException exception) {
            LOGGER.debug("Can't get Contact by Id {} {}", id, exception);
            throw exception;
        }
        return editedPhoneNumber;
    }

    @Override
    public void deleteById(int id) {
        LOGGER.debug("DAO Delete PhoneNumber by id");
        try (SqlSession sqlSession = getSession()) {
            try {
                getPhoneNumberMapper(sqlSession).deleteById(id);
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't delete PhoneNumber by id", exception);
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All PhoneNumbers");
        try (SqlSession sqlSession = getSession()) {
            try {
                getPhoneNumberMapper(sqlSession).deleteAll();
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't delete all PhoneNumbers", exception);
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
    }
}
