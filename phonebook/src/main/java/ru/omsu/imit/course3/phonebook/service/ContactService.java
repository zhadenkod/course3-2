package ru.omsu.imit.course3.phonebook.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.phonebook.dao.ContactDAO;
import ru.omsu.imit.course3.phonebook.daoimpl.ContactDAOImpl;
import ru.omsu.imit.course3.phonebook.models.Contact;
import ru.omsu.imit.course3.phonebook.rest.request.ContactRequest;
import ru.omsu.imit.course3.phonebook.rest.responce.ContactInfoResponse;
import ru.omsu.imit.course3.phonebook.rest.responce.EmptySuccessResponse;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;
import ru.omsu.imit.course3.phonebook.utils.PhonebookUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class ContactService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContactService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private ContactDAO contactDAO = new ContactDAOImpl();

    public Response insertContact(String json) {
        LOGGER.debug("Insert contact " + json);
        try {
            ContactRequest request = PhonebookUtils.getClassInstanceFromJson(GSON, json, ContactRequest.class);
            Contact contact = new Contact(request.getFirstname(), request.getLastname(), request.getSection_id());
            Contact addedContact = contactDAO.insert(contact);
            String response = GSON.toJson(new ContactInfoResponse(addedContact.getId(),
                    addedContact.getFirstname(), addedContact.getLastname(), addedContact.getSection().getId()));
            System.out.println(response);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response getById(int id) {
        LOGGER.debug("Get contact by id " + id);
        try {
            Contact contact = contactDAO.getById(id);
            String response = GSON.toJson(new ContactInfoResponse(contact.getId(),
                    contact.getFirstname(), contact.getLastname(), contact.getSection().getId()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response getAll() {
        LOGGER.debug("Get all contacts");
        List<Contact> contactList = contactDAO.getAll();
        List<ContactInfoResponse> responseList = new ArrayList<>();
        for (Contact contact : contactList) {
            responseList.add(new ContactInfoResponse(contact.getId(),
                    contact.getFirstname(), contact.getLastname(), contact.getSection().getId()));
        }
        String response = GSON.toJson(responseList);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response editById(int id, String json) {
        LOGGER.debug("Edit by id " + id);
        try {
            ContactRequest request = PhonebookUtils.getClassInstanceFromJson(GSON, json, ContactRequest.class);
            Contact editedContact = contactDAO.editContact(id, request.getFirstname(), request.getLastname());
            String response = GSON.toJson(new ContactInfoResponse(editedContact.getId(),
                    editedContact.getFirstname(), editedContact.getLastname(), editedContact.getSection().getId()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response deleteById(int id) {
        LOGGER.debug("Delete by id " + id);
        try {
            contactDAO.deleteById(id);
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException ex) {
            return PhonebookUtils.failureResponse(ex);
        }
    }

    public Response deleteAll() {
        LOGGER.debug("Delete all");
        contactDAO.deleteAll();
        String response = GSON.toJson(new EmptySuccessResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

}
