package ru.omsu.imit.course3.phonebook.rest.request;

import ru.omsu.imit.course3.phonebook.models.PhoneNumberType;

public class PhoneNumberRequest {
    private String phoneNumber;
    private PhoneNumberType phoneNumberType;
    private int contact_id;

    protected PhoneNumberRequest() {}

    public PhoneNumberRequest(String phoneNumber, PhoneNumberType phoneNumberType,
                              int contact_id) {
        this.phoneNumber = phoneNumber;
        this.phoneNumberType = phoneNumberType;
        this.contact_id = contact_id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public PhoneNumberType getPhoneNumberType() {
        return phoneNumberType;
    }

    public int getContact_id() {
        return contact_id;
    }
}
