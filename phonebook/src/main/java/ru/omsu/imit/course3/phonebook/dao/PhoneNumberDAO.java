package ru.omsu.imit.course3.phonebook.dao;

import ru.omsu.imit.course3.phonebook.models.PhoneNumber;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;

import java.util.List;

public interface PhoneNumberDAO {
    PhoneNumber insert(PhoneNumber phoneNumber);

    PhoneNumber getById(int id) throws PhonebookException;

    List<PhoneNumber> getAll();

    PhoneNumber getByContact(int contact_id) throws PhonebookException;

    PhoneNumber editPhoneNumber(int id, String phoneNumber) throws PhonebookException;

    void deleteById(int id) throws PhonebookException;

    void deleteAll();

}
