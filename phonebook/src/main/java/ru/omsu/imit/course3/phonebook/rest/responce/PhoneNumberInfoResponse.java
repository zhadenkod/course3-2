package ru.omsu.imit.course3.phonebook.rest.responce;

public class PhoneNumberInfoResponse {
    private int id;
    private String phoneNumber;
    private String phoneNumberType;
    private int contact_id;

    public PhoneNumberInfoResponse(int id, String phoneNumber, String phoneNumberType, int contact_id) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.phoneNumberType = phoneNumberType;
        this.contact_id = contact_id;
    }

    protected PhoneNumberInfoResponse() {}

    public int getId() {
        return id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhoneNumberType() {
        return phoneNumberType;
    }

    public int getContact_id() {
        return contact_id;
    }
}
