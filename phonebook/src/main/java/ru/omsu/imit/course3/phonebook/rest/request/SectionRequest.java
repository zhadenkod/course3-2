package ru.omsu.imit.course3.phonebook.rest.request;

public class SectionRequest {
    private String name;

    public SectionRequest(String name) {
        super();
        this.name = name;
    }

    protected SectionRequest() {}

    public String getName() {
        return name;
    }
}
