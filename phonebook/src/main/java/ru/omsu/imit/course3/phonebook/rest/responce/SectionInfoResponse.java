package ru.omsu.imit.course3.phonebook.rest.responce;

public class SectionInfoResponse {
    private int id;
    private String name;

    public SectionInfoResponse(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    protected SectionInfoResponse() {}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
