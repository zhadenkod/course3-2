package ru.omsu.imit.course3.phonebook.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.phonebook.dao.SectionDAO;
import ru.omsu.imit.course3.phonebook.daoimpl.SectionDAOImpl;
import ru.omsu.imit.course3.phonebook.models.Section;
import ru.omsu.imit.course3.phonebook.rest.request.SectionRequest;
import ru.omsu.imit.course3.phonebook.rest.responce.EmptySuccessResponse;
import ru.omsu.imit.course3.phonebook.rest.responce.SectionInfoResponse;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;
import ru.omsu.imit.course3.phonebook.utils.PhonebookUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class SectionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SectionService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private SectionDAO sectionDAO = new SectionDAOImpl();

    public Response insertSection(String json) {
        LOGGER.debug("Insert section " + json);
        try {
            SectionRequest request = PhonebookUtils.getClassInstanceFromJson(GSON, json, SectionRequest.class);
            Section section = new Section(request.getName());
            Section addedSection = sectionDAO.insert(section);
            String response = GSON.toJson(new SectionInfoResponse(addedSection.getId(), addedSection.getName()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response getById(int id) {
        LOGGER.debug("Get section by id " + id);
        try {
            Section section = sectionDAO.getById(id);
            String response = GSON.toJson(new SectionInfoResponse(section.getId(), section.getName()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response getAll() {
        LOGGER.debug("Get all sections");
        List<Section> sectionList = sectionDAO.getAll();
        List<SectionInfoResponse> responseList = new ArrayList<>();
        for (Section section : sectionList) {
            responseList.add(new SectionInfoResponse(section.getId(), section.getName()));
        }
        String response = GSON.toJson(responseList);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response editById(int id, String json) {
        LOGGER.debug("Edit by id " + id);
        try {
            SectionRequest request = PhonebookUtils.getClassInstanceFromJson(GSON, json, SectionRequest.class);
            Section section = sectionDAO.getById(id);
            Section editedSection = sectionDAO.edit(id, request.getName());
            String response = GSON.toJson(new SectionInfoResponse(editedSection.getId(), editedSection.getName()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response deleteById(int id) {
        LOGGER.debug("Delete by id " + id);
        try {
            sectionDAO.deleteById(id);
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response deleteAll() {
        LOGGER.debug("Delete all");
        sectionDAO.deleteAll();
        String response = GSON.toJson(new EmptySuccessResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

}
