package ru.omsu.imit.course3.phonebook.rest.request;

public class ContactRequest {
    private String firstname;
    private String lastname;
    private int section_id;

    protected ContactRequest() {}

    public ContactRequest(String firstname, String lastname,
                          int section_id) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.section_id = section_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public int getSection_id() {
        return section_id;
    }
}
