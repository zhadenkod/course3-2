package ru.omsu.imit.course3.phonebook.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.phonebook.dao.CommonDAO;

public class CommonDAOImpl extends BaseDAOImpl implements CommonDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseDAOImpl.class);

    @Override
    public void clear() {
        LOGGER.debug("Clear Database");
        try (SqlSession sqlSession = getSession()) {
            try {
                getPhoneNumberMapper(sqlSession).deleteAll();
                getContactMapper(sqlSession).deleteAll();
                getSectionMapper(sqlSession).deleteAll();
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't clear database");
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
    }
}
