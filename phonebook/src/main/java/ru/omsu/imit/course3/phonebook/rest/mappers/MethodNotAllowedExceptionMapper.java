package ru.omsu.imit.course3.phonebook.rest.mappers;

import ru.omsu.imit.course3.phonebook.utils.ErrorCode;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;
import ru.omsu.imit.course3.phonebook.utils.PhonebookUtils;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class MethodNotAllowedExceptionMapper implements	ExceptionMapper<NotAllowedException> {

    @Override
	public Response toResponse(NotAllowedException exception) {
		return PhonebookUtils.failureResponse(Status.METHOD_NOT_ALLOWED, new PhonebookException(ErrorCode.METHOD_NOT_ALLOWED));
	}
}