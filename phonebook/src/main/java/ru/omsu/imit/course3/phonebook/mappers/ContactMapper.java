package ru.omsu.imit.course3.phonebook.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.course3.phonebook.models.Contact;

import java.util.List;

public interface ContactMapper {
    @Insert("INSERT INTO contacts (firstname, lastname, section_id) VALUES (#{firstname}, #{lastname}, #{section.id})")
    @Options(useGeneratedKeys = true)
    Integer insert(Contact contact);

    @Select("SELECT * FROM contacts")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "section.id", column = "section_id"),
            @Result(property = "phoneNumbers", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.course3.phonebook.mappers.PhoneNumberMapper.getByContact", fetchType = FetchType.LAZY) )
    })
    List<Contact> getAll();

    @Select("SELECT id, firstname, lastname, section_id FROM contacts WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "section.id", column = "section_id"),
            @Result(property = "phoneNumbers", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.course3.phonebook.mappers.PhoneNumberMapper.getByContact", fetchType = FetchType.LAZY) )
    })
    Contact getById(int id);

    @Select("UPDATE contacts SET firstname = #{firstname} , lastname = #{lastname} WHERE id = #{id}")
    void edit(@Param("id")int id, @Param("firstname") String firstname, @Param("lastname") String lastname);

    @Delete("DELETE FROM contacts WHERE id = #{id}")
    void deleteById(int id);

    @Delete("DELETE FROM contacts")
    void deleteAll();
}
