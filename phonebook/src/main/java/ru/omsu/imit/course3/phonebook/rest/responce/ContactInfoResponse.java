package ru.omsu.imit.course3.phonebook.rest.responce;

public class ContactInfoResponse {
    private int id;
    private String firstname;
    private String lastname;
    private int section_id;

    public ContactInfoResponse(int id, String firstname, String lastname, int section_id) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.section_id = section_id;
    }

    protected ContactInfoResponse() {}

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public int getSection_id() {
        return section_id;
    }
}
