package ru.omsu.imit.course3.phonebook.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.phonebook.dao.SectionDAO;
import ru.omsu.imit.course3.phonebook.models.Section;
import ru.omsu.imit.course3.phonebook.utils.ErrorCode;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;

import java.util.List;

public class SectionDAOImpl extends BaseDAOImpl implements SectionDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(SectionDAOImpl.class);

    @Override
    public Section insert(Section section) {
        LOGGER.debug("DAO Insert Section {}", section);
        try (SqlSession sqlSession = getSession()) {
            try {
                getSectionMapper(sqlSession).insert(section);
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't insert Section {} {}", section, exception);
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
        return section;
    }

    @Override
    public Section getById(int id) throws PhonebookException {
        LOGGER.debug("DAO get section by id");
        Section section;
        try (SqlSession sqlSession = getSession()) {
            section = getSectionMapper(sqlSession).getById(id);
            if (section == null) {
                LOGGER.debug("Can't get Section by Id {}", id);
                throw new PhonebookException(ErrorCode.SECTION_NOT_FOUND, Integer.toString(id));
            }
            sqlSession.commit();
        } catch (RuntimeException exception) {
            LOGGER.debug("Can't get Section by Id {} {}", id, exception);
            throw exception;
        }
        return section;
    }

    @Override
    public List<Section> getAll() {
        LOGGER.debug("DAO get All Sections");
        List<Section> result;
        try (SqlSession sqlSession = getSession()) {
            try {
                result = getSectionMapper(sqlSession).getAll();
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't get all Sections {}", exception);
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
        return result;
    }

    @Override
    public Section edit(int id, String name) {
        LOGGER.debug("DAO edit section by id");
        Section editedSection;
        try (SqlSession sqlSession = getSession()) {
            try {
                getSectionMapper(sqlSession).edit(id, name);
                editedSection = getSectionMapper(sqlSession).getById(id);
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't edit Section by id {}", exception);
                sqlSession.rollback();
                throw exception;
            }
        }
        return editedSection;
    }

    @Override
    public void deleteById(int id) {
        LOGGER.debug("DAO delete section by id");
        try (SqlSession sqlSession = getSession()) {
            try {
                getSectionMapper(sqlSession).deleteById(id);
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't delete Section by id {}", exception);
                sqlSession.rollback();
                throw exception;
            }
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Sections {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getSectionMapper(sqlSession).deleteAll();
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't delete all Sections {}", exception);
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
    }
}
