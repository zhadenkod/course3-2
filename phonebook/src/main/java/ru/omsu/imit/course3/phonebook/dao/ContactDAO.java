package ru.omsu.imit.course3.phonebook.dao;

import ru.omsu.imit.course3.phonebook.models.Contact;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;

import java.util.List;

public interface ContactDAO {
    Contact insert(Contact contact);

    List<Contact> getAll();

    Contact getById(int id) throws PhonebookException;

    Contact editContact(int id, String firstname,
                        String lastname) throws PhonebookException;

    void deleteById(int id) throws PhonebookException;

    void deleteAll();
}
