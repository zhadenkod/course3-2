package ru.omsu.imit.course3.phonebook.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Contact {
    private int id;
    private String firstname;
    private String lastname;
    private Section section;
    private List<PhoneNumber> phoneNumbers;

    public Contact() {}

    public Contact(int id, String firstname, String lastname, Section section) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.section = section;
        this.phoneNumbers = new ArrayList<>();
    }

    public Contact(String firstname, String lastname, int section) {
        this.id = 0;
        this.firstname = firstname;
        this.lastname = lastname;
        this.section = new Section(section, "");
        this.phoneNumbers = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public void addPhoneNumber(PhoneNumber phoneNumber) {
        phoneNumbers.add(phoneNumber);
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", section=" + section +
                ", phoneNumbers=" + phoneNumbers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return id == contact.id &&
                Objects.equals(section, contact.section) &&
                Objects.equals(firstname, contact.firstname) &&
                Objects.equals(lastname, contact.lastname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, lastname, section);
    }
}
