package ru.omsu.imit.course3.phonebook.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.phonebook.dao.ContactDAO;
import ru.omsu.imit.course3.phonebook.models.Contact;
import ru.omsu.imit.course3.phonebook.utils.ErrorCode;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;

import java.util.List;

public class ContactDAOImpl extends BaseDAOImpl implements ContactDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContactDAOImpl.class);

    @Override
    public Contact insert(Contact contact) {
        LOGGER.debug("DAO Insert Contact {}", contact);
        try (SqlSession sqlSession = getSession()) {
            try {
                 getContactMapper(sqlSession).insert(contact);
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't insert Contact {} {}", contact, exception);
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
        return contact;
    }

    @Override
    public List<Contact> getAll() {
        try (SqlSession sqlSession = getSession()) {
            return getContactMapper(sqlSession).getAll();
        }
        catch (RuntimeException exception) {
            LOGGER.debug("Can't get all Contacts", exception);
            throw exception;
        }
    }

    @Override
    public Contact getById(int id) throws PhonebookException {
        try (SqlSession sqlSession = getSession()) {
            Contact contact = getContactMapper(sqlSession).getById(id);
            if (contact == null) {
                LOGGER.debug("Can't get Contact by Id {}", id);
                throw new PhonebookException(ErrorCode.CONTACT_NOT_FOUND, Integer.toString(id));
            }
            return contact;
        }
        catch (RuntimeException exception) {
            LOGGER.debug("Can't get Contact by Id {} {}", id, exception);
            throw exception;
        }
    }

    @Override
    public Contact editContact(int id, String firstname, String lastname) throws PhonebookException {
        Contact editedContact;
        try (SqlSession sqlSession = getSession()) {
            getContactMapper(sqlSession).edit(id, firstname, lastname);
            editedContact = getContactMapper(sqlSession).getById(id);
            if (editedContact == null) {
                LOGGER.debug("Can't get Contact by Id {}", id);
                throw new PhonebookException(ErrorCode.CONTACT_NOT_FOUND, Integer.toString(id));
            }
        }
        catch (RuntimeException exception) {
            LOGGER.debug("Can't get Contact by Id {} {}", id, exception);
            throw exception;
        }
        return editedContact;
    }

    @Override
    public void deleteById(int id) throws PhonebookException {
        LOGGER.debug("DAO Delete All Contacts {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getContactMapper(sqlSession).deleteAll();
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't delete all Contacts {}", exception);
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Contacts {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getContactMapper(sqlSession).deleteAll();
            } catch (RuntimeException exception) {
                LOGGER.debug("Can't delete all Contacts {}", exception);
                sqlSession.rollback();
                throw exception;
            }
            sqlSession.commit();
        }
    }
}
