package ru.omsu.imit.course3.phonebook.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.phonebook.dao.PhoneNumberDAO;
import ru.omsu.imit.course3.phonebook.daoimpl.PhoneNumberDAOImpl;
import ru.omsu.imit.course3.phonebook.models.PhoneNumber;
import ru.omsu.imit.course3.phonebook.rest.request.PhoneNumberRequest;
import ru.omsu.imit.course3.phonebook.rest.responce.EmptySuccessResponse;
import ru.omsu.imit.course3.phonebook.rest.responce.PhoneNumberInfoResponse;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;
import ru.omsu.imit.course3.phonebook.utils.PhonebookUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class PhoneNumberService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContactService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private PhoneNumberDAO phoneNumberDAO = new PhoneNumberDAOImpl();

    public Response insertPhoneNumber(String json) {
        LOGGER.debug("Insert phone number " + json);
        try {
            PhoneNumberRequest request = PhonebookUtils.getClassInstanceFromJson(GSON, json, PhoneNumberRequest.class);
            PhoneNumber phoneNumber = new PhoneNumber(request.getPhoneNumber(), request.getPhoneNumberType(),
                    request.getContact_id());
            PhoneNumber addedPhoneNumber = phoneNumberDAO.insert(phoneNumber);
            String response = GSON.toJson(new PhoneNumberInfoResponse(addedPhoneNumber.getId(),
                    addedPhoneNumber.getPhoneNumber(), addedPhoneNumber.getType().toString(), addedPhoneNumber.getContact().getId()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response getById(int id) {
        LOGGER.debug("Get phone number by id " + id);
        try {
            PhoneNumber phoneNumber = phoneNumberDAO.getById(id);
            String response = GSON.toJson(new PhoneNumberInfoResponse(phoneNumber.getId(),
                    phoneNumber.getPhoneNumber(), phoneNumber.getType().toString(), phoneNumber.getContact().getId()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response getAll() {
        LOGGER.debug("Get all phone numbers");
        List<PhoneNumber> phoneNumberList = phoneNumberDAO.getAll();
        List<PhoneNumberInfoResponse> responseList = new ArrayList<>();
        for (PhoneNumber phoneNumber : phoneNumberList) {

            responseList.add(new PhoneNumberInfoResponse(phoneNumber.getId(),
                    phoneNumber.getPhoneNumber(), phoneNumber.getType().toString(), phoneNumber.getContact().getId()));
        }
        String response = GSON.toJson(responseList);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response editById(int id, String json) {
        LOGGER.debug("Edit by id " + id);
        try {
            PhoneNumberRequest request = PhonebookUtils.getClassInstanceFromJson(GSON, json, PhoneNumberRequest.class);
            PhoneNumber editedPhoneNumber = phoneNumberDAO.editPhoneNumber(id, request.getPhoneNumber());
            String response = GSON.toJson(new PhoneNumberInfoResponse(editedPhoneNumber.getId(),
                    editedPhoneNumber.getPhoneNumber(), editedPhoneNumber.getType().toString(), editedPhoneNumber.getContact().getId()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException exception) {
            return PhonebookUtils.failureResponse(exception);
        }
    }

    public Response deleteById(int id) {
        LOGGER.debug("Delete by id " + id);
        try {
            phoneNumberDAO.deleteById(id);
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PhonebookException ex) {
            return PhonebookUtils.failureResponse(ex);
        }
    }

    public Response deleteAll() {
        LOGGER.debug("Delete all");
        phoneNumberDAO.deleteAll();
        String response = GSON.toJson(new EmptySuccessResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
}
