package ru.omsu.imit.course3.phonebook.dao;

import ru.omsu.imit.course3.phonebook.models.Section;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;

import java.util.List;

public interface SectionDAO {
    Section insert(Section section);

    Section getById(int id) throws PhonebookException;

    List<Section> getAll();

    Section edit(int id, String name) throws PhonebookException;

    void deleteById(int id) throws PhonebookException;

    void deleteAll();
}
