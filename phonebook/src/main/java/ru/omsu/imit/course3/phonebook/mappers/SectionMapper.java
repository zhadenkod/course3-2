package ru.omsu.imit.course3.phonebook.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.course3.phonebook.models.Section;

import java.util.List;

public interface SectionMapper {
    @Insert("INSERT INTO sections (name) VALUES (#{name})")
    @Options(useGeneratedKeys = true)
    Integer insert(Section section);

    @Select("SELECT id, name FROM sections WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "contacts", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.course3.phonebook.mappers.ContactMapper.getById", fetchType = FetchType.LAZY) )
    })
    Section getById(int id);

    @Select("SELECT id, name FROM sections")
    List<Section> getAll();

    @Update("UPDATE sections SET name = #{name} WHERE id = #{id}")
    void edit(@Param("id") int id, @Param("name") String name);

    @Delete("DELETE FROM sections WHERE id = #{id}")
    void deleteById(int id);

    @Delete("DELETE FROM sections")
    void deleteAll();
}
