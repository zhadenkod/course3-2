package ru.omsu.imit.course3.phonebook.rest.mappers;

import ru.omsu.imit.course3.phonebook.utils.ErrorCode;
import ru.omsu.imit.course3.phonebook.utils.PhonebookException;
import ru.omsu.imit.course3.phonebook.utils.PhonebookUtils;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class WrongURLExceptionMapper implements	ExceptionMapper<NotFoundException> {

    @Override
	public Response toResponse(NotFoundException exception) {
		return PhonebookUtils.failureResponse(Status.NOT_FOUND, new PhonebookException(ErrorCode.WRONG_URL));
	}
}