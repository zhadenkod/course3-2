package ru.omsu.imit.course3.phonebook.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.course3.phonebook.rest.responce.FailureResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class PhonebookUtils {
    private static final Gson GSON = new Gson();

    public static <T> T getClassInstanceFromJson(Gson gson, String json, Class<T> clazz) throws PhonebookException {
        if (StringUtils.isEmpty(json)) {
            throw new PhonebookException(ErrorCode.NULL_REQUEST);
        }
        try {
            return gson.fromJson(json, clazz);
        } catch (JsonSyntaxException ex) {
            throw new PhonebookException(ErrorCode.JSON_PARSE_EXCEPTION, json);
        }
    }

    public static Response failureResponse(Response.Status status, PhonebookException ex) {
        return Response.status(status).entity(GSON.toJson(new FailureResponse(ex.getErrorCode(), ex.getMessage())))
                .type(MediaType.APPLICATION_JSON).build();
    }

    public static Response failureResponse(PhonebookException exception) {
        return failureResponse(Response.Status.BAD_REQUEST, exception);
    }
}
