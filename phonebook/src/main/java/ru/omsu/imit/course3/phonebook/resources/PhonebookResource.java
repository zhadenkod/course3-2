package ru.omsu.imit.course3.phonebook.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import ru.omsu.imit.course3.phonebook.service.ContactService;
import ru.omsu.imit.course3.phonebook.service.PhoneNumberService;
import ru.omsu.imit.course3.phonebook.service.SectionService;


@Path("/api")
public class PhonebookResource {
	private static SectionService sectionService = new SectionService();
	private static ContactService contactService = new ContactService();
	private static PhoneNumberService phoneNumberService = new PhoneNumberService();

    @POST
    @Path("/sections")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addSection(String json) {
		return sectionService.insertSection(json);
    }

    @POST
    @Path("/contacts")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addContact(String json) {
		return contactService.insertContact(json);
    }

    @POST
    @Path("/phonenumbers")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addPhoneNumber(String json) {
        return phoneNumberService.insertPhoneNumber(json);
    }

    @GET
    @Path("/sections")
    @Produces("application/json")
    public Response getSections() {
        return sectionService.getAll();
    }

    @GET
    @Path("/contacts")
    @Produces("application/json")
    public Response getContacts() {
        return contactService.getAll();
    }

    @GET
    @Path("/phonenumbers")
    @Produces("application/json")
    public Response getPhoneNumbers() {
        return phoneNumberService.getAll();
    }

    @GET
    @Path("/sections/{id}")
    @Produces("application/json")
    public Response getSectionById(@PathParam(value = "id") int id) {
		return sectionService.getById(id);
    }

    @GET
    @Path("/contacts/{id}")
    @Produces("application/json")
    public Response getContactById(@PathParam(value = "id") int id) {
        return contactService.getById(id);
    }

    @GET
    @Path("/phonenumbers/{id}")
    @Produces("application/json")
    public Response getPhoneNumberById(@PathParam(value = "id") int id) {
        return phoneNumberService.getById(id);
    }

    @PUT
    @Path("/sections/{id}")
    @Produces("application/json")
    public Response editSectionById(@PathParam(value = "id") int id, String json) {
		return sectionService.editById(id, json);
    }

    @PUT
    @Path("/contacts/{id}")
    @Produces("application/json")
    public Response editContactById(@PathParam(value = "id") int id, String json) {
        return contactService.editById(id, json);
    }

    @PUT
    @Path("/phonenumbers/{id}")
    @Produces("application/json")
    public Response editPhoneNumberById(@PathParam(value = "id") int id, String json) {
        return phoneNumberService.editById(id, json);
    }

    @DELETE
    @Path("/sections/{id}")
    @Produces("application/json")
    public Response deleteSectionById(@PathParam(value = "id") int id) {
		return sectionService.deleteById(id);
    }

    @DELETE
    @Path("/contacts/{id}")
    @Produces("application/json")
    public Response deleteContactById(@PathParam(value = "id") int id) {
        return contactService.deleteById(id);
    }

    @DELETE
    @Path("/phonenumbers/{id}")
    @Produces("application/json")
    public Response deletePhoneNumberById(@PathParam(value = "id") int id) {
        return phoneNumberService.deleteById(id);
    }

}
