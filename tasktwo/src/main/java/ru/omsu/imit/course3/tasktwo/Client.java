package ru.omsu.imit.course3.tasktwo;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] args) {
        final int serverPort = 6666;
        final String address = "localhost";
        InetAddress ipAddress;
        try {
            ipAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            return;
        }
        try (Socket socket = new Socket(ipAddress, serverPort);
            DataInputStream in = new DataInputStream(socket.getInputStream());
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in, "CP866"))) {

            System.out.println(socket.getLocalPort());
            String line;
            System.out.println("Try to guess a number from 1 to 1000");

            while (true) {
                line = keyboardReader.readLine();
                out.writeUTF(line);
                out.flush();
                if (line.equalsIgnoreCase("quit")) {
                    System.out.println("Client stopped");
                    break;
                }
                line = in.readUTF();
                System.out.println("Server answer is: " + line);
                if (line.equals("Угадал")) {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }
}
