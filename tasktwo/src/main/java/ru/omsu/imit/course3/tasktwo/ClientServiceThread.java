package ru.omsu.imit.course3.tasktwo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientServiceThread extends Thread {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientServiceThread.class);

    private Socket clientSocket;
    private int clientID;
    private int number;

    public ClientServiceThread(Socket socket, int id) {
        clientSocket = socket;
        clientID = id;
        number = (int) (1 + Math.random() * 1000);
    }

    public void run() {
        LOGGER.info(
                "Accepted Client: ID - " + clientID + ": Address - " + clientSocket.getInetAddress().getHostName());
        int tryCount = 0;
        try (DataInputStream in = new DataInputStream(clientSocket.getInputStream());
             DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream())) {

            while (true) {
                String clientCommand = in.readUTF();
                if (clientCommand.equalsIgnoreCase("quit")) {
                    LOGGER.info("Stopping client thread for client: " + clientID);
                    LOGGER.info("Trying count: " + tryCount);
                    break;
                } else {
                    if (Integer.valueOf(clientCommand) > number) {
                        out.writeUTF("Много");
                        out.flush();
                        tryCount++;
                    } else if (Integer.valueOf(clientCommand) < number) {
                        out.writeUTF("Мало");
                        out.flush();
                        tryCount++;
                    } else {
                        out.writeUTF("Угадал");
                        out.flush();
                        tryCount++;
                        LOGGER.info("Stopping client thread for client: " + clientID);
                        LOGGER.info("Trying count: " + tryCount);
                        break;
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        finally {
            LOGGER.info("Closing socket");
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
