package ru.omsu.imit.course3.taskone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class JDBCDemo {
    private static final String URL = "jdbc:mysql://localhost:3306/bookstore";
    private static final String USER = "root";
    private static final String PASSWORD = "test";
    private static final Logger LOGGER = LoggerFactory.getLogger(JDBCDemo.class);

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            LOGGER.error("Error loading JDBC Driver ");
            LOGGER.error(e.getMessage());
            return false;
        }
    }

    private static void insertBook(Connection con, Book book) {
        String sql = "INSERT INTO books VALUES(NULL,?,?,?)";
        try (PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
             ResultSet keys = stmt.getGeneratedKeys()) {
            if (keys.next()) {
                stmt.setNull(1, keys.getInt(1));
            }
            stmt.setString(1, book.getTitle());
            stmt.setInt(2, book.getYear());
            stmt.setInt(3, book.getPages());
            stmt.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

    }

    private static void getAllBooks(Connection con) {
        String query = "SELECT * FROM books";
        System.out.println("Books : ");
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                Book book = new Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        System.out.println("End Books");
    }

    private static void getFirstBooks(Connection con) {
        String query = "SELECT * FROM books LIMIT 3";
        System.out.println("Books : ");
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                Book book = new Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        System.out.println("End Books");
    }

    private static void getFirstBooksById(Connection con) {
        String query = "SELECT * FROM books WHERE id < 2";
        System.out.println("Books : ");
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                Book book = new Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        System.out.println("End Books");
    }

    private static void getBooksByRequest(Connection con, String query) {
        System.out.println("Books : ");
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                Book book = new Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        System.out.println("End Books");
    }

    private static void updateAllBooksPages(Connection con) {
        String updateQuery = "UPDATE books SET pages = ?";
        try (PreparedStatement stmt = con.prepareStatement(updateQuery)) {
            stmt.setInt(1, 400);
            stmt.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

    }

    private static void deleteBook(Connection con, String query) {
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public static void main(String[] args) {
        if (!loadDriver()) {
            return;
        }
        String selectQuery = "select * from books";

        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            insertBook(con, new Book(1, "Java", 2000, 1000));
            insertBook(con, new Book(1, "Гарри Поттер и философский камень", 2002, 399));
            insertBook(con, new Book(1, "C++", 2010, 1500));
            insertBook(con, new Book(1, "Отцы и дети", 1979, 240));
            insertBook(con, new Book(1, "Недоросль", 2013, 240));

            getAllBooks(con);
            getFirstBooks(con);
            getFirstBooksById(con);
            getBooksByRequest(con,"SELECT * FROM books WHERE title = \"Отцы и дети\"");
            getBooksByRequest(con,"SELECT * FROM books WHERE title LIKE \"Отцы%\"");
            getBooksByRequest(con,"SELECT * FROM books WHERE title LIKE \"%ь\"");
            getBooksByRequest(con,"SELECT * FROM books WHERE YEAR BETWEEN 2010 AND 2015");
            getBooksByRequest(con,"SELECT * FROM books WHERE pages BETWEEN 235 AND 400 AND YEAR BETWEEN 2010 AND 2015");

            updateAllBooksPages(con);
            getAllBooks(con);

            deleteBook(con, "DELETE FROM books WHERE title LIKE \"Ja%\"");
            getAllBooks(con);
            deleteBook(con, "DELETE FROM books WHERE id BETWEEN 2 AND 5");
            getAllBooks(con);
            deleteBook(con, "DELETE FROM books WHERE title = \"Java\" OR title = \"Гарри\"");
            getAllBooks(con);
            deleteBook(con, "DELETE FROM books");
            getAllBooks(con);

        } catch (SQLException sqlEx) {
            LOGGER.error(sqlEx.getMessage());
        }
    }

}
